package main

import (
	"os"
	"strings"
	"errors"
	"io/ioutil"
	"strconv"
)

func addChatToAllowedChats(filename string, chatID int64) error {
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString("\n" + strconv.FormatInt(chatID, 10))
	return err
}


func addChannelToWhitelist(filename string, channel string) error{
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString("\n" + channel)
	return err
}


func removeChatFromAllowedChats(filename string, chatID int64) error {
	textChatID := strconv.FormatInt(chatID,10)
	data, err := ioutil.ReadFile(filename)
	if err!= nil {
		return err
	}

	lines := strings.Split(string(data), "\n")
	found := false
	var newLines []string

	for _, line := range lines {
		if line != textChatID {
			newLines = append(newLines, line)
		} else {
			found = true
		}
	}

	if !found {
		return errors.New("chatID not found in chat list")
	}

	newData := strings.Join(newLines, "\n")
	err = ioutil.WriteFile(filename, []byte(newData), 0666)
	if err != nil {
		return err
	}

	return nil
}


func removeFromWhitelist(channelToRemove string) error {
	data, err := ioutil.ReadFile("whitelist.txt")
	if err!= nil {
		return err
	}

	lines := strings.Split(string(data), "\n")
	found := false
	var newLines []string

	for _, line := range lines {
		if line != channelToRemove {
			newLines = append(newLines, line)
		} else {
			found = true
		}
	}

	if !found {
		return errors.New("channel not found in whitelist")
	}

	newData := strings.Join(newLines, "\n")
	err = ioutil.WriteFile("whitelist.txt", []byte(newData), 0666)
	if err != nil {
		return err
	}

	return nil
}
