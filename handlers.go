package main

import (
  tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
  "fmt"
  "log"
	"strings"
	"io/ioutil"
	"time"
  "strconv"
)


func handleListCommand(bot *tgbotapi.BotAPI, message *tgbotapi.Message, filename string){
	if message.Chat.Type == "private" && allowedUsers[message.From.ID] {
		data, err := ioutil.ReadFile(filename)
		if err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, fmt.Sprintf("Error reading whitelist: %v", err))
			bot.Send(msg)
			return
		}

		lines := strings.Split(string(data), "\n")
		whitelistString := strings.Join(lines, "\n")
		if whitelistString == "" {
			whitelistString = "The whitelist is empty."
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, "Whitelist contents: \n\n"+whitelistString)
		bot.Send(msg)
	} else {
		msg := tgbotapi.NewMessage(message.Chat.ID, "You are not authorized to use this command.")
		bot.Send(msg)
	}
}


func handleTempAddCommand(bot *tgbotapi.BotAPI, message *tgbotapi.Message, whitelist map[string]bool) {
  if message.Chat.Type == "private" && allowedUsers[message.From.ID] {
  args := strings.Split(message.Text, " ")
  if len(args) < 3 {
    msg := tgbotapi.NewMessage(message.Chat.ID, "Usage: /tempadd @channelusername duration_in_minutes")
    _, _ = bot.Send(msg)
    return
}

  duration, err := strconv.Atoi(args[2])
  if err != nil || duration <= 0 || duration > 30 {
    msg := tgbotapi.NewMessage(message.Chat.ID, "Duration must be an integer between 1 and 30 minutes")
    _, _ = bot.Send(msg)
    return
  }

  channel := args[1]

	if channel != "" {
			whitelist[channel] = true
			err := addChannelToWhitelist("whitelist.txt", channel)
			if err != nil {
				log.Printf("Error updating whitelist: %v", err)
			} else {
				msg := tgbotapi.NewMessage(message.Chat.ID, "Channel temporarily added to whitelist: "+channel)
				bot.Send(msg)

				go func() {
					time.Sleep(time.Duration(duration) * time.Minute)
					err := removeFromWhitelist(channel)
					if err != nil {
						log.Printf("Error removing channel from whitelist: %v", err)
					} else {
						removeFromWhitelist(channel)
						log.Printf("Channel %s removed from whitelist after %d minutes", channel, duration)
						msg:= tgbotapi.NewMessage(message.Chat.ID, "Temporary channel was removed: "+channel)
						bot.Send(msg)
					}
				}()
			}
		}
	} else  if !allowedUsers[message.From.ID] {
      msg := tgbotapi.NewMessage (message.Chat.ID, "You are not authorized to use this command")
      _, _ = bot.Send(msg)
      return
    }
}

func handleRemoveCommand(bot *tgbotapi.BotAPI, message *tgbotapi.Message, whitelist map[string]bool) {
  if message.From.ID == allowedUser && message.Chat.Type == "private"{
    channel := strings.TrimSpace(strings.TrimPrefix(message.Text, "/remove"))
    if channel != "" {
      err := removeFromWhitelist(channel)
      if err != nil {
        log.Printf("Error removing channel from whitelist: %v", err)
      } else {
        removeFromWhitelist(channel)
        msg := tgbotapi.NewMessage (message.Chat.ID, "Channel removed from whitelist: "+channel)
        bot.Send(msg)
      }
    }
  } else if message.Chat.Type != "private" || message.From.ID != allowedUser {
    msg := tgbotapi.NewMessage (message.Chat.ID, "You are not authorized to use this command")
    bot.Send(msg)
  }
}


func handleWhitelistCommand(bot *tgbotapi.BotAPI, message *tgbotapi.Message, whitelist map[string]bool) {
	if allowedUsers[message.From.ID] && message.Chat.Type == "private"{
		channel := strings.TrimSpace(strings.TrimPrefix(message.Text, "/whitelist"))
		if channel != "" {
			whitelist[channel] = true
			err := addChannelToWhitelist("whitelist.txt", channel)
			if err != nil {
				log.Printf("Error updating whitelist: %v", err)
			} else {
				msg := tgbotapi.NewMessage (message.Chat.ID, "Channel added to whitelist: "+channel)
				bot.Send(msg)
			}
		}
	} else if message.Chat.Type != "private" || message.From.ID != allowedUser {
		msg := tgbotapi.NewMessage (message.Chat.ID, "You are not authorized to use this command")
		bot.Send(msg)
	}
}


func handleEnableBotCommand(bot *tgbotapi.BotAPI, message *tgbotapi.Message, allowedChats map[int64]bool) {
	if message.Chat.Type == "group" && allowedUsers[message.From.ID] {
		allowedChats[message.Chat.ID] = true
		err := addChatToAllowedChats("chats.txt", message.Chat.ID)
		if err != nil {
			log.Printf("Error updating allowed chats: %v", err)
		} else {
			msg := tgbotapi.NewMessage(message.Chat.ID, "Bot enabled in this chat.")
			bot.Send(msg)
		}
	}
}

func handleDisableBotCommand(bot *tgbotapi.BotAPI, message *tgbotapi.Message, allowedChats map[int64]bool) {
	if message.Chat.Type == "group" && allowedUsers[message.From.ID] {
		err := removeChatFromAllowedChats("chats.txt", message.Chat.ID)
		if err != nil {
			log.Printf("Error updating allowed chats: %v", err)
		} else {
			msg := tgbotapi.NewMessage(message.Chat.ID, "Bot disabled in this chat.")
			bot.Send(msg)
		}
	}
}
