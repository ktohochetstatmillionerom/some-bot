package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
	"strings"
)

const allowedUser int64 = 193117018

var allowedUsers = map[int64]bool{
	193117018: true,
	85208468: true,
}


func main() {
	token, err := readBotToken("token.txt")
	if err != nil {
		log.Panicf("No BOT_TOKEN environment variable is set")
	}
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}

	whitelist, err := readWhitelist("whitelist.txt")
	if err != nil {
		log.Panicf("Error reading whitelist: %v", err)
	}
	allowedChats, err := readAllowedChats("chats.txt")
	if err != nil {
		log.Panicf("Error reading allowed chats: %v", err)
	}
	bot.Debug = false

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message != nil {
			m := update.Message
			if m.From == nil {
				return
			}

			if strings.HasPrefix(m.Text, "/enable_bot") {
				handleEnableBotCommand(bot, m, allowedChats)
			}  else if strings.HasPrefix(m.Text, "/disable_bot") {
				handleDisableBotCommand(bot, m, allowedChats)
			} else if strings.HasPrefix(m.Text, "/whitelist") && m.Chat.Type == "private" {
				handleWhitelistCommand(bot, m, whitelist)
			} else if strings.HasPrefix(m.Text, "/tempadd") && m.Chat.Type == "private" {
				handleTempAddCommand(bot, m, whitelist)
			} else if strings.HasPrefix(m.Text, "/list") && m.Chat.Type == "private" {
				handleListCommand(bot, m, "whitelist.txt")
			} else if m.Text != "" && strings.HasPrefix(m.Text, "/remove") && m.Chat.Type == "private" {
				handleRemoveCommand(bot, m, whitelist)
			}

			if m.SenderChat != nil && m.SenderChat.Type == "channel" {
  			if !allowedChats[m.Chat.ID] {
      		return
  			}
				} else if m.SenderChat != nil && !whitelist[m.SenderChat.UserName] {
    				_, err := bot.Send(tgbotapi.DeleteMessageConfig{
        		ChatID:    update.Message.Chat.ID,
        		MessageID: update.Message.MessageID,
    			})
    		if err != nil {
        	if m.From != nil {
            log.Printf("[%s] %s, err: %s", update.Message.From.UserName, update.Message.Text, err.Error())
        	} else {
            log.Printf("[unknown] %s, err: %s", update.Message.Text, err.Error())
        	}
        	return
    		}
  		}
		}
	}
}
