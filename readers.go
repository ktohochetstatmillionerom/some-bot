package main

import (
	"os"
	"bufio"
	"strings"
	"fmt"
	"strconv"
)

func readBotToken(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer file.Close()

	scanner := bufio.NewScanner (file)
	if scanner.Scan() {
		return strings.TrimSpace(scanner.Text()), nil
	}

	return "", fmt.Errorf("no token found in %s", filename)
}

func readWhitelist(filename string) (map[string]bool, error) {
	file, err := os.Open(filename)
	if err != nil{
		return nil, err
	}
	defer file.Close()

	whitelist := make(map[string]bool)
	scanner :=bufio.NewScanner(file)
	for scanner.Scan(){
		channel := scanner.Text()
		whitelist[channel] = true
	}

	return whitelist, scanner.Err()
}

func readAllowedChats(filename string) (map[int64]bool, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	allowedChats := make(map[int64]bool)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		chatID, err := strconv.ParseInt(scanner.Text(), 10, 64)
		if err == nil {
			allowedChats[chatID] = true
		}
	}
  return allowedChats, scanner.Err()
}
